#!/bin/bash
#Script to get all repositories under a user from bitbucket
#Based and forked by http://haroldsoh.com/2011/10/07/clone-all-repos-from-a-bitbucket-source/

user="user"
password="pass"
local_repo="/destnation/folder"
teams="team1 team2 teamN"

date=`date`

echo "Start on: " $date
 
#USER REPOS

curl -u $user:$password https://api.bitbucket.org/1.0/users/$user > repoinfo
 
for repo_name in `cat repoinfo | sed -r 's/("name": )/\n\1/g' | sed -r 's/"name": "(.*)"/\1/' | sed -e 's/{//' | cut -f1 -d\" | tr '\n' ' '`
	do
	    	echo "Cloning " $repo_name
	    	#git clone https://$user@bitbucket.org/$user/$repo_name $local_repo/$repo_name
	    	git clone git@bitbucket.org:$user/$repo_name $local_repo/$repo_name
	 	echo "---"
	done

#REPOS TEAMS

for repo_team in $teams

	do

		curl -u $user:$password https://api.bitbucket.org/1.0/users/$repo_team > repoinfoteam

		for repo_name in `cat repoinfoteam | sed -r 's/("name": )/\n\1/g' | sed -r 's/"name": "(.*)"/\1/' | sed -e 's/{//' | cut -f1 -d\" | tr '\n' ' '`
		do
		    	echo "Cloning " $repo_name
		   	#git clone https://$user@bitbucket.org/$teams/$repo_name $local_repo/$repo_name
		    	git clone git@bitbucket.org:$repo_team/$repo_name $local_repo/$repo_name
		 	echo "---"
		done
	done

exit 0
